import { IcoteiPage } from './app.po';

describe('icotei App', function() {
  let page: IcoteiPage;

  beforeEach(() => {
    page = new IcoteiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
