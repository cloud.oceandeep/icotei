import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.scss']
})
export class DashComponent implements OnInit {


  listActivits = [
    {order: "04899432", type: "in-separation", percentage: "25", value: "3.920,00"},
    {order: "04899432", type: "waiting-approval", percentage: "25", value: "3.920,00"},
    {order: "04899432", type: "delivered", percentage: "25", value: "3.920,00"},
    {order: "04899432", type: "delivere-pending", percentage: "25", value: "3.920,00"},
  ]

  listActivitsPending = [
    {order: "04899432", type: "in-separation", percentage: "25", value: "3.920,00"},
    {order: "04899432", type: "waiting-approval", percentage: "25", value: "3.920,00"},
    {order: "04899432", type: "delivered", percentage: "25", value: "3.920,00"},
    {order: "04899432", type: "delivere-pending", percentage: "25", value: "3.920,00"},
  ]

  constructor() { }

  ngOnInit() {
  }

}
