import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.scss']
})
export class DeliveryComponent implements OnInit {

  listProduct = [
    {img: "https://www.drogariaminasbrasil.com.br/media/product/766/energetico-monster-energy-lata-473ml-5f5.jpg", name: "Energético Monster Lata 473ml", qtd: 15},
    {img: "https://www.carone.com.br/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/2/7/27333_B.jpg", name: "Fanta", qtd: 20},
    {img: "https://savegnago.vteximg.com.br/arquivos/ids/445306-1000-1000/CEREAL-MATINAL-KELLOGGS-SUCRILHOS-730G-T.jpg?v=637241911619470000", name: "Sucrilhos", qtd: 31},
    {img: "https://images-na.ssl-images-amazon.com/images/I/51qQ0MQ3rAL._AC_SX342_.jpg", name: "Brahma", qtd: 18}
  ]

  constructor() { }

  ngOnInit() {
  }

  setNegative(i) {
    if(this.listProduct[i].qtd !== 0) {
      this.listProduct[i].qtd = this.listProduct[i].qtd - 1;
    }    
  }

  setPositive(i) {
    this.listProduct[i].qtd = this.listProduct[i].qtd + 1;
  }

  removeItem(i) {
    this.listProduct.splice(i, 1);
  }

}
