import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-header-checkout',
  templateUrl: './header-checkout.component.html',
  styleUrls: ['./header-checkout.component.scss']
})
export class HeaderCheckoutComponent implements OnInit {

  @Input() activeStep: string = "1";
  activeStep1: boolean;
  activeStep2: boolean;
  activeStep3: boolean;
  constructor() { }

  ngOnInit() {
    if (this.activeStep == '1') {
      this.activeStep1 = true
    }
    if (this.activeStep == '2') {
      this.activeStep2 = true
    }
    if (this.activeStep == '3') {
      this.activeStep2 = true
    }
  }

}
