import { Component, OnInit, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  @Input() menuActive: boolean = false;
  @Input() iconsMenu: boolean = true;
  itemDropMenu: boolean;

  constructor() { }

  ngOnInit() {}

  openMenu() {
    this.menuActive = !this.menuActive;
  }

  openDropMenu(event) {    
    if(event.srcElement.classList[3] == "drop-active") {
      event.srcElement.classList.remove("drop-active");
    } else {
      event.srcElement.classList.add("drop-active");
    }
    
    
  }


}
