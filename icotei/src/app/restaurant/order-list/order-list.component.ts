import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {

  listActivits = [
    {order: "04899432", type: "in-separation", percentage: "25", value: "3.920,00"},
    {order: "04899432", type: "waiting-approval", percentage: "25", value: "3.920,00"},
    {order: "04899432", type: "delivered", percentage: "25", value: "3.920,00"},
    {order: "04899432", type: "delivere-pending", percentage: "25", value: "3.920,00"},
  ]

  constructor() { }

  ngOnInit() {
  }

}
