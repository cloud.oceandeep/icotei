/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { OrderCreateService } from './order-create.service';

describe('OrderCreateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrderCreateService]
    });
  });

  it('should ...', inject([OrderCreateService], (service: OrderCreateService) => {
    expect(service).toBeTruthy();
  }));
});
