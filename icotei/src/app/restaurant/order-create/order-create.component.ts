import { Component, EventEmitter, OnInit } from '@angular/core';

import { AlertService } from '../alert/alert.service';

@Component({
  selector: 'app-order-create',
  templateUrl: './order-create.component.html',
  styleUrls: ['./order-create.component.scss']
})
export class OrderCreateComponent implements OnInit {

  msgAlert: string;
  typeAlert: string;
  activeAlert: boolean;

  listProduct = [
    {img: "https://www.drogariaminasbrasil.com.br/media/product/766/energetico-monster-energy-lata-473ml-5f5.jpg", name: "Energético Monster Lata 473ml", qtd: 0},
    {img: "https://www.carone.com.br/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/2/7/27333_B.jpg", name: "Fanta", qtd: 0},
    {img: "https://savegnago.vteximg.com.br/arquivos/ids/445306-1000-1000/CEREAL-MATINAL-KELLOGGS-SUCRILHOS-730G-T.jpg?v=637241911619470000", name: "Sucrilhos", qtd: 0},
    {img: "https://images-na.ssl-images-amazon.com/images/I/51qQ0MQ3rAL._AC_SX342_.jpg", name: "Brahma", qtd: 0},
    {img: "https://api.tendaatacado.com.br/fotos/6235_mini.jpg", name: "Arroz", qtd: 0},
    {img: "https://fibraalimentos.fbitsstatic.net/img/p/leite-uht-integral-tirol-1l-116240/302214.jpg?w=500&h=500&v=no-change", name: "Leite Tirol", qtd: 0}
  ]

  constructor(
    public alertService: AlertService
  ) {  }

  ngOnInit() {
  }

  setNegative(i) {
    if(this.listProduct[i].qtd !== 0) {
      this.listProduct[i].qtd = this.listProduct[i].qtd - 1;
    }    
  }

  setPositive(i) {
    this.listProduct[i].qtd = this.listProduct[i].qtd + 1;
  }

  addProduct(i) {
    this.msgAlert =  this.listProduct[i].name + " adicionado(a) com sucesso";
    this.typeAlert = "success";
    this.activeAlert = true;

    this.listProduct[i].qtd = 0;
    this.alertService.getAlert(this.msgAlert, this.typeAlert, this.activeAlert);
  }

}
