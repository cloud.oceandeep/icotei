import { Component, OnInit, Input} from '@angular/core';
import { from } from 'rxjs/observable/from';
import { MenuComponent } from '../menu/menu.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() iconsMenu: boolean = true;

  menuActive = false;
  shoppingActive = false;

  constructor() { }

  ngOnInit() {  }

  openMenu() {
    this.menuActive = !this.menuActive;
  }

  openShopping() {
    this.shoppingActive = !this.shoppingActive;
  }

}
