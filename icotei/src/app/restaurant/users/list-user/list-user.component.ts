import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {

  listUsers = [
    {status: true, code: "001", name: "Ricardo Loretto", email: "ricardolmora78@gmail.com", user: "ricardo.loretto"},
    {status: true, code: "002", name: "Ricardo Loretto", email: "ricardolmora78@gmail.com", user: "ricardo.loretto"},
    {status: true, code: "003", name: "Ricardo Loretto", email: "ricardolmora78@gmail.com", user: "ricardo.loretto"},
    {status: false, code: "004", name: "Ricardo Loretto", email: "ricardolmora78@gmail.com", user: "ricardo.loretto"},
    {status: true, code: "005", name: "Ricardo Loretto", email: "ricardolmora78@gmail.com", user: "ricardo.loretto"}
  ]

  constructor() { }

  ngOnInit() {
  }

}
