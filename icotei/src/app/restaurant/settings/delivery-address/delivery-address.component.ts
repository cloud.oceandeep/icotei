import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delivery-address',
  templateUrl: './delivery-address.component.html',
  styleUrls: ['./delivery-address.component.scss']
})
export class DeliveryAddressComponent implements OnInit {

  listAddress = [
    {nameAddress: "Restaurante Unidade Leblon", active: true, address: "Endereço: Av. Gen. San Martin, 889 - Leblon, Rio de Janeiro - RJ", cep: "22441-015"},
    {nameAddress: "Restaurante Unidade Leblon", active: false, address: "Endereço: Av. Gen. San Martin, 889 - Leblon, Rio de Janeiro - RJ", cep: "22441-015"}
  ]

  constructor() { }

  ngOnInit() {
  }

}
