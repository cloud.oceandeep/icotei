import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-preference',
  templateUrl: './preference.component.html',
  styleUrls: ['./preference.component.scss']
})
export class PreferenceComponent implements OnInit {

  p1: boolean;
  p2: boolean;
  p3: boolean;

  constructor() { }

  ngOnInit() {
    this.p1 = false;
    this.p2 = true;
    this.p3 = false;
  }

  changeStatus(event) {
    if (event == 'p1') {
      this.p1 = !this.p1;
    } else if(event == 'p2') {
      this.p2 = !this.p2;
    } else if(event == 'p3') {
      this.p3 = !this.p3;
    }
  }


}
