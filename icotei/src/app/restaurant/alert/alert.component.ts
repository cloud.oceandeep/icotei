import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';

import { AlertService } from './alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  alert: Subscription;
  alertMsg;
  alertType;
  alertActive;

  constructor(
    private alertService: AlertService
  ) {  }

  ngOnInit() {
    this.alert = this.alertService.obterAlert().subscribe(data => [this.alertMsg = data.msg, this.alertType = data.type, this.alertActive = data.active]);
  } 

  closeAlert() {
    this.alertActive = !this.alertActive;
  }

  // ngOnDestroy() {
  //   this.alert = this.alertService.obterAlert().value;
  // }

  // get musica(): string {
  //   return this.alertService.obterAlert().value;
  // }
}
