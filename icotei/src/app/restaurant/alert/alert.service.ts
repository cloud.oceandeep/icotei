import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()

export class AlertService {

  private alert = new BehaviorSubject<any>('');

  constructor() { }

  getAlert(msg: string, type: string, active: boolean) {
    this.alert.next({msg, type, active});
  }

  obterAlert() {
    return this.alert;
  }

}