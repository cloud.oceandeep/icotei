import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ErrorComponent } from './error/error.component';

import { RouterModule } from '@angular/router';
import { DashComponent } from './restaurant/dash/dash.component';
import { HeaderComponent } from './restaurant/header/header.component';
import { MenuComponent } from './restaurant/menu/menu.component';
import { RegistrationDataComponent } from './restaurant/settings/registration-data/registration-data.component';
import { DeliveryAddressComponent } from './restaurant/settings/delivery-address/delivery-address.component';
import { AddAddressComponent } from './restaurant/settings/delivery-address/add-address/add-address.component';
import { PreferenceComponent } from './restaurant/settings/preference/preference.component';
import { ActivityHistoryComponent } from './restaurant/settings/activity-history/activity-history.component';
import { UsersComponent } from './restaurant/users/users.component';
import { ListUserComponent } from './restaurant/users/list-user/list-user.component';
import { AddUserComponent } from './restaurant/users/add-user/add-user.component';
import { OrderListComponent } from './restaurant/order-list/order-list.component';
import { OrderCreateComponent } from './restaurant/order-create/order-create.component';
import { AlertComponent } from './restaurant/alert/alert.component';

import { AlertService } from './restaurant/alert/alert.service';
import { ShoppingCartComponent } from './restaurant/shopping-cart/shopping-cart.component';

import { DeliveryComponent } from './restaurant/checkout/delivery/delivery.component';
import { HeaderCheckoutComponent } from './restaurant/checkout/header-checkout/header-checkout.component';
import { QuotationComponent } from './restaurant/checkout/quotation/quotation.component';
import { CheckOutComponent } from './restaurant/checkout/check-out/check-out.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ErrorComponent,
    DashComponent,
    HeaderComponent,
    MenuComponent,
    RegistrationDataComponent,
    DeliveryAddressComponent,
    AddAddressComponent,
    PreferenceComponent,
    ActivityHistoryComponent,
    UsersComponent,
    ListUserComponent,
    AddUserComponent,
    OrderListComponent,
    OrderCreateComponent,
    AlertComponent,
    ShoppingCartComponent,
    DeliveryComponent,
    HeaderCheckoutComponent,
    QuotationComponent,
    CheckOutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {path: '', component: LoginComponent},
      {path: 'login', component: LoginComponent},
      {path: 'restaurante', component: DashComponent},
      {path: 'restaurante/dados-cadastrais', component: RegistrationDataComponent},
      {path: 'restaurante/endereco-entrega', component: DeliveryAddressComponent},
      {path: 'restaurante/endereco-entrega/adicionar', component: AddAddressComponent},
      
      {path: 'restaurante/lista-de-pedidos', component: OrderListComponent},
      {path: 'restaurante/criar-pedido', component: OrderCreateComponent},

      {path: 'restaurante/pedido/entrega', component: DeliveryComponent},
      {path: 'restaurante/pedido/cotacao', component: QuotationComponent},
      {path: 'restaurante/pedido/checkout', component: CheckOutComponent},

      {path: 'restaurante/preferencia', component: PreferenceComponent},
      {path: 'restaurante/historico-de-atividades', component: ActivityHistoryComponent},

      {path: 'restaurante/lista-de-usuarios', component: ListUserComponent},
      {path: 'restaurante/usuarios', component: AddAddressComponent},
      {path: '**', component: ErrorComponent},
    ]),
  ],
  providers: [AlertService],
  bootstrap: [AppComponent]
})
export class AppModule { }
